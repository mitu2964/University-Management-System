<?php

namespace App\AllocateClassroom;
use PDO;
class classroom
{
    private $id = "";
    private $dept_id = "";
    private $course_id = "";
    private $room_id = "";
    private $day_id = "";
    private $from = "";
    private $to = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=varsitysystem', 'root', '');

    }
    public function setData($data='')
    {
        if (array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if (array_key_exists('dept_id',$data))
        {
            $this->dept_id = $data['dept_id'];
        }
        if (array_key_exists('course_id',$data))
        {
            $this->course_id = $data['course_id'];
        }
        if(array_key_exists('room_id',$data)){
            $this->room_id = $data['room_id'];
        }
        if(array_key_exists('day_id',$data)){
            $this->day_id = $data['day_id'];
        }
        if(array_key_exists('from',$data)){
            $this->from = $data['from'];
        }
        if(array_key_exists('to',$data)){
            $this->to = $data['to'];
        }
        return $this;
    }
    public function DepartmentView()
    {
        try{
            $query = "SELECT * FROM `departments` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $dept = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $dept;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function RoomView()
    {
        try{
            $query = "SELECT * FROM `rooms` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $room = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $room;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function DayView()
    {
        try{
            $query = "SELECT * FROM `days` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $day = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $day;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function check()
    {
        $query = "SELECT * FROM  course_class_mapping order by id DESC";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        print_r($data);
        return $data;
    }
    public function store()
    {
        try {
            $query = "INSERT INTO `course_class_mapping` (`id`, `dept_id`,`course_id`,`room_id`, `day_id`,`from`,`to`,`created_at`) 
                              VALUES (:id,:dept_id,:course,:room,:day,:from,:to,:create)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':dept_id' => $this->dept_id,
                    ':course' => $this->course_id,
                    ':room'=>$this->room_id,
                    ':day' => $this->day_id,
                    ':from' => $this->from,
                    ':to' => $this->to,
                    ':create'=> date('Y-m-d h:m:s'),
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Stored";
                header("location:create.php");
            }else{
                $_SESSION['message'] = "Failed";
                header("location:create.php");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }
}