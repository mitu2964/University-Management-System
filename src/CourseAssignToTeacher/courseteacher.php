<?php

namespace App\CourseAssignToTeacher;

use PDO;
class courseteacher
{
    private $id = "";
    private $dept_id = "";
    private $teacher_id = "";
    private $total_credit = "";
    private $remain_credit = "";
    private $course_code = "";
    private $course_name = "";
    private $course_credit_c = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=varsitysystem', 'root', '');

    }
    public function setData($data='')
    {
        if (array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if (array_key_exists('dept_id',$data))
        {
            $this->dept_id = $data['dept_id'];
        }
        if (array_key_exists('teacher_id',$data))
        {
            $this->teacher_id = $data['teacher_id'];
        }
        if(array_key_exists('total_credit',$data)){
            $this->total_credit = $data['total_credit'];
        }
        if(array_key_exists('remain_credit',$data)){
            $this->remain_credit = $data['remain_credit'];
        }
        if(array_key_exists('course_code',$data)){
            $this->course_code = $data['course_code'];
        }
        if(array_key_exists('course_name',$data)){
            $this->course_name = $data['course_name'];
        }
        if(array_key_exists('course_credit_c',$data)){
            $this->course_credit_c = $data['course_credit_c'];
        }
        return $this;
    }
    public function DepartmentView()
    {
        try{
            $query = "SELECT * FROM `departments` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $dept = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $dept;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function store()
    {
        try {
            $query = "INSERT INTO `course_teacher_mapping` (`id`, `dept_id`,`teacher_id`,`total_credit`, `remain_credit`,`course_code`,`course_name`,`course_credit_c`,`created_at`) 
                              VALUES (:id,:dept_id,:teacher_id,:total,:remain,:code,:name,:credit,:create)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':dept_id' => $this->dept_id,
                    ':teacher_id' => $this->teacher_id,
                    ':total'=>$this->total_credit,
                    ':remain' => $this->remain_credit,
                    ':code' => $this->course_code,
                    ':name' => $this->course_name,
                    ':credit' => $this->course_credit_c,
                    ':create'=> date('Y-m-d h:m:s'),
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Stored";
                header("location:create.php");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }
}