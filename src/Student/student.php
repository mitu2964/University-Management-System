<?php

namespace App\Student;
use PDO;
class student
{
    private $id = "";
    private $title = "";
    private $address = "";
    private $mail = "";
    private $date="";
    private $contact = "";
    private $department = "";
    private $student_id = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=varsitysystem', 'root', '');

    }
    public function setData($data='')
    {
        if (array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('title',$data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('address',$data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('mail',$data)){
            $this->mail = $data['mail'];
        }
        if(array_key_exists('contact',$data)){
            $this->contact = $data['contact'];
        }
        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
        if(array_key_exists('department',$data)){
            $this->department = $data['department'];
        }
        if(array_key_exists('student_id',$data)){
            $this->student_id = $data['student_id'];
        }
        return $this;
    }
    public function DepartmentView()
    {
        try{
            $query = "SELECT * FROM `departments` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $dept = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $dept;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function store()
    {
        try {
            $query = "INSERT INTO `students` (`id`,`title`,`address`,`mail`,`contact`,`department`,`date`,`student_id`,`created_at`) VALUES (:id,:title,:add,:mail,:contact,:dept,:date,:std_id,:create)";
            $stmnt = $this->pdo->prepare($query);
            if(!empty($this->department)){
                $numofRows = $this->pdo->query("SELECT * FROM students,departments WHERE department='$this->department'")->rowCount();
                $nRows = $numofRows+1;
                if ($nRows<10){
                    $this->student_id = preg_replace("/[^A-Z]+/", "", $this->department)."-".date('Y')."-"."00".$nRows;
                }
                elseif ($nRows>= 10 && $nRows<=99){
                    $this->student_id = preg_replace("/[^A-Z]+/", "", $this->department)."-".date('Y')."-"."0".$nRows;
                }
                elseif ($nRows>= 100){
                    $this->student_id = preg_replace("/[^A-Z]+/", "", $this->department)."-".date('Y')."-".$nRows;
                }
            }
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':title' => $this->title,
                    ':add' => $this->address,
                    ':mail' => $this->mail,
                    ':contact' => $this->contact,
                    ':dept' => $this->department,
                    ':date' => $this->date,
                    ':std_id'=>$this->student_id,
                    ':create'=> date('Y-m-d h:m:s'),
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Stored";
                header("location:create.php");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }
    public function show()
    {
        try{
            $query = "SELECT * FROM `students` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function view()
    {
        try{
            $query = "SELECT * FROM `students` WHERE id = $this->id";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $query = "UPDATE `students` SET `title`=:title,`address`=:add,`mail`=:mail,`contact`=:contact,`department`=:department,`date`=:date,`updated_at`=:update WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(
                ':id' => $this->id,
                ':title'=>$this->title,
                ':add'=>$this->address,
                ':mail'=>$this->mail,
                ':contact'=>$this->contact,
                ':date'=>$this->date,
                ':department'=>$this->department,
                ':update'=>date('Y-m-d h:m:s'),
            ));
            if ($stmt) {
                $_SESSION['update-message'] = "Successfully Data Updated";
                header("location:edit.php?id=$this->id");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $query = "DELETE FROM students WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Student/view.php");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}