<?php

namespace App\Department;

use PDO;
class department
{
    private $id = "";
    private $code = "";
    private $title = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=varsitysystem', 'root', '');

    }

    public function setData($data='')
    {
        if (array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('code',$data)){
            $this->code =strtoupper( $data['code']);
        }
        if(array_key_exists('title',$data)){
            $this->title = ucwords(strtolower($data['title']));
        }
        return $this;
    }
    public function store()
    {
            try {
                $query = "INSERT INTO `departments` (`id`, `code`, `title`,`created_at`) VALUES (:id,:code,:title,:create)";
                $stmnt = $this->pdo->prepare($query);
                $stmnt->execute(
                    array(
                        ':id' => null,
                        ':code' => $this->code,
                        ':title' => $this->title,
                        ':create' => date('Y-m-d h:m:s')
                    )
                );
                if ($stmnt) {
                    $_SESSION['message'] = "Successfully Data Stored";
                    header("location:create.php");
                }
            } catch (PDOException $e) {
                echo 'Error' . $e->getMessage();
            }
    }
    public function validation(){

    }
    public function show()
    {
        try{
            $query = "SELECT * FROM `departments` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function view()
    {
        try{
            $query = "SELECT * FROM `departments` WHERE id = $this->id";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $query = "UPDATE `departments` SET `code`=:code,`title`=:title,`updated_at`=:update WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(
                ':id'   => $this->id,
                ':code'=>$this->code,
                ':title'=>$this->title,
                ':update'=>date('Y-m-d h:m:s'),
            ));
            if ($stmt) {
                $_SESSION['update-message'] = "Successfully Data Updated";
                header("location:edit.php?id=$this->id");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $query = "DELETE FROM departments WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Department/view.php");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}