<?php

namespace App\Teacher;
use PDO;
class teacher
{
    private $id = "";
    private $dept_id = "";
    private $designation_id = "";
    private $title = "";
    private $address = "";
    private $mail = "";
    private $contact = "";
    private $credit = "";
    private $pdo = "";

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=varsitysystem', 'root', '');

    }
    public function setData($data='')
    {
        if (array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if (array_key_exists('dept_id',$data))
        {
            $this->dept_id = $data['dept_id'];
        }
        if (array_key_exists('designation_id',$data))
        {
            $this->designation_id = $data['designation_id'];
        }
        if(array_key_exists('title',$data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('address',$data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('mail',$data)){
            $this->mail = $data['mail'];
        }
        if(array_key_exists('contact',$data)){
            $this->contact = $data['contact'];
        }
        if(array_key_exists('credit',$data)){
            $this->credit = $data['credit'];
        }
        return $this;
    }
    public function DesignationView()
    {
        try{
            $query = "SELECT * FROM `designations` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $semester = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $semester;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function DepartmentView()
    {
        try{
            $query = "SELECT * FROM `departments` ";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $dept = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $dept;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function store()
    {
        try {
            $query = "INSERT INTO `teachers` (`id`, `dept_id`,`designation_id`,`title`,`address`,`mail`,`contact`,`credit`,`created_at`) VALUES (:id,:dept_id,:designation_id,:title,:add,:mail,:contact,:credit,:create)";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute(
                array(
                    ':id' => null,
                    ':dept_id' => $this->dept_id,
                    ':designation_id' => $this->designation_id,
                    ':title' => $this->title,
                    ':add' => $this->address,
                    ':mail' => $this->mail,
                    ':contact' => $this->contact,
                    ':credit' => $this->credit,
                    ':create'=> date('Y-m-d h:m:s'),
                )
            );
            if ($stmnt) {
                $_SESSION['message'] = "Successfully Data Stored";
                header("location:create.php");
            }
        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }
    public function show()
    {
        try{
            $query = "SELECT teachers.id as id,teachers.title as title,teachers.address as address,teachers.mail as mail,teachers.contact as contact,teachers.credit as credit,designations.title as designation_title,departments.title as dept_title FROM `teachers` LEFT JOIN `designations` ON teachers.designation_id = designations.id LEFT JOIN `departments` ON teachers.dept_id=departments.id";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetchAll();
            return $value;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function view()
    {
        try{
            $query = "SELECT teachers.id as teacher_id,teachers.title as title, teachers.address as address, teachers.mail as mail, teachers.contact as contact, teachers.credit as credit,departments.title as dept_title,departments.id as dept_id FROM `teachers`,`departments` WHERE teachers.id="."'".$this->id ."'"."AND teachers.dept_id=departments.id";
            $stmnt = $this->pdo->prepare($query);
            $stmnt->execute();
            $value = $stmnt->fetch(PDO::FETCH_ASSOC);

            $query1 = "SELECT teachers.id as teacher_id, designations.id as designation_id,designations.title as designation_title FROM `teachers`,`designations` WHERE teachers.id="."'".$this->id ."'"."AND teachers.designation_id=designations.id";
            $stmnt = $this->pdo->prepare($query1);
            $stmnt->execute();
            $value1 = $stmnt->fetch(PDO::FETCH_ASSOC);
            $data=[
                'department'=>$value,
                'designation'=>$value1,
            ];
            return $data;
        }catch (PDOException $e) {
            echo  'Error' . $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $query = "UPDATE `teachers` SET `dept_id`=:dept,`designation_id`=:designation_id,`title`=:title,`address`=:add,`mail`=:mail,`contact`=:contact,`credit`=:credit,`updated_at`=:update WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(
                ':id' => $this->id,
                ':dept' => $this->dept_id,
                ':designation_id' => $this->designation_id,
                ':title'=>$this->title,
                ':add'=>$this->address,
                ':mail'=>$this->mail,
                ':contact'=>$this->contact,
                ':credit'=>$this->credit,
                ':update'=>date('Y-m-d h:m:s'),
            ));
            if ($stmt) {
                $_SESSION['update-message'] = "Successfully Data Updated";
                header("location:edit.php?id=$this->id");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $query = "DELETE FROM teachers WHERE id = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "Successfully Data Deleted";
                header("location:../Teacher/view.php");
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}