<?php
include_once ("../../vendor/autoload.php");
use App\CourseAssignToTeacher\courseteacher;
$obj = new courseteacher();
$obj->setData($_GET);
$value = $obj->DepartmentView();

?>
<?php include_once"../header.php"; ?>

<?php include_once("../Admin/side-menubar.php"); ?>

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span
                            class="text-semibold">Home</span> - Dashboard</h4>

                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                    <li><a style="color: #0a001f;" href="view.php">Course List</a></li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-md-8">

                    <!-- Basic layout-->
                    <form action="store.php" class="form-horizontal" method="post">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h2 class="panel-title" >Course-Assign-To-Teacher<button style="margin-left: 20px" class="btn btn-success" ><?php
                                        if (isset($_SESSION['message'])){
                                            echo $_SESSION['message'];
                                            unset($_SESSION['message']);
                                        } ?></button></h2>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Department:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="dept_id" id="dept_id" class="form-control">
                                            <option value="">Select Department</option>
                                            <?php
                                            foreach ($value as $item) {
                                                ?>
                                                <option value="<?php echo $item['id'];?>"><?php echo $item['title'];?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Teacher:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" id="teacher_id" name="teacher_id" class="form-control">
                                            <option value="">Select Teacher</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Credit To Be Taken:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="total_credit" name="total_credit" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Remaining Credit:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="remain_credit" name="remain_credit" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Code:</label>
                                    <div class="col-lg-9">
                                        <select data-placeholder="Select your state" name="course_code" id="course_code" class="form-control">
                                            <optgroup label="Course">
                                                <option value="">Select Course Code</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Name:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="course_name" name="course_name" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Credit:</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="course_credit_c" name="course_credit_c" readonly>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Assign<i class="icon-arrow-right14 position-right"></i></button>
                                </div>

                            </div>
                        </div>
                    </form>
                    <!-- /basic layout -->
                </div>

<?php
  include_once("../footer.php");
?>

<!--    		--><?php
//	} else{
//		$_SESSION['fail']= "You are not authorized!";
//		header('location:../../../index.php');
//	}

?>