<?php
$db = new PDO('mysql:host=localhost;dbname=varsitysystem', 'root', '');

    if (isset($_POST['depart'])){
        $dept_id =$_POST['depart'];
        $query = "SELECT * FROM `teachers` WHERE teachers.dept_id=".$dept_id;
        $stmnt = $db->prepare($query);
        $stmnt->execute();
        $teacher = $stmnt->fetchAll(PDO::FETCH_ASSOC);

        $query2 = "SELECT * FROM `courses` WHERE courses.dept_id=".$dept_id;
        $stmnt2 = $db->prepare($query2);
        $stmnt2->execute();
        $course = $stmnt2->fetchAll(PDO::FETCH_ASSOC);

        $data = [
            'teacher'=>$teacher,
            'course'=>$course,
        ];

        echo json_encode($data);
    }
    if (isset($_POST['teacher_id'])){
        $teacher_id =$_POST['teacher_id'];
        $query = "SELECT * FROM `teachers` WHERE teachers.id=".$teacher_id;
        $stmnt = $db->prepare($query);
        $stmnt->execute();
        $teacher = $stmnt->fetch(PDO::FETCH_ASSOC);

        $query2 = "SELECT SUM(course_credit_c) as assign_credit FROM course_teacher_mapping WHERE teacher_id=".$teacher_id;
        $stmnt2 = $db->prepare($query2);
        $stmnt2->execute();
        $assign_credit = $stmnt2->fetch(PDO::FETCH_ASSOC);

        $data = [
            'teacher'=>$teacher,
            'a_credit'=>$assign_credit,
        ];

        echo json_encode($data);
    }
    if (isset($_POST['course_id']))
    {
        $course_id=$_POST['course_id'];

        $sql = "SELECT * FROM `courses` WHERE id=".$course_id;
        $stm = $db->prepare($sql);
        $stm->execute();
        $data = $stm->fetch();

        echo json_encode($data);
    }
    if (isset($_POST['dept_id']))
    {
        $department_id=$_POST['dept_id'];

        $sql2 = "SELECT courses.code as code, courses.title as title, semesters.title as semester FROM `courses`,`semesters` 
                  where dept_id='".$department_id ."' AND courses.semester_id=semesters.id";
        $stm2 = $db->prepare($sql2);
        $stm2->execute();
        $course_n = $stm2->fetchAll(PDO::FETCH_ASSOC);

        $sql3 ="SELECT teachers.title as title , course_teacher_mapping.course_code 
                FROM teachers INNER JOIN course_teacher_mapping ON teachers.id=course_teacher_mapping.teacher_id 
                where teachers.dept_id=".$department_id;
        $stm3 = $db->prepare($sql3);
        $stm3->execute();
        $t_title = $stm3->fetchAll(PDO::FETCH_ASSOC);

        $new_t = [];
        foreach ($course_n as $key => $data) {

            if (array_key_exists($key, $t_title) ) {
                $new_t[$key]['title'] = $t_title[$key]['title'];
            }else{
                $new_t[$key]['title'] = "<b>Not Assigned</b>";
            }
        }

        $data = [
            'courses'=>$course_n,
            'teacher_title'=>$new_t,
        ];

        echo json_encode($data);

    }
