<?php
include_once ("../../vendor/autoload.php");
use App\Department\department;
$obj = new department();
$obj->setData($_GET);
$value = $obj->view();
?>
<?php
//if (!empty($_SESSION['user_info'])) {
//    ?>
    <?php include_once"../header.php"; ?>


    <?php include_once("../Admin/side-menubar.php"); ?>



    <!-- Main content -->
    <div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span
                        class="text-semibold">Home</span> - Dashboard</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="../Admin/dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Dashboard</li>
                <li><a style="color: #0a001f;" href="view.php">Department List</a></li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
    <div class="row">
    <div class="col-md-8">

        <!-- Basic layout-->
        <form action="update.php" class="form-horizontal" method="post">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h2 class="panel-title"> Update Your Data<?php
                            if (isset($_SESSION['update-message'])){
                                ?>
                        <span class="label label-success position-right" style="font-size: 14px">
                                <?php
                                echo $_SESSION['update-message'];
                                unset($_SESSION['update-message']);
                            } ?></span></h2>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Code:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" value="<?php echo $value['code'];?>" placeholder="Department Code" name="code"required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Name:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" value="<?php echo $value['title'];?>" placeholder="Department Name" name="title"required>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="<?php echo $value['id']; ?>">
        </form>
        <!-- /basic layout -->
    </div>
    </div>
        <!-- /content area -->

    </div>
    </div>
    <!-- /main content -->

    <?php
    include_once("../footer.php");
    ?>

<!--    --><?php
//} else{
//    $_SESSION['fail']= "You are not authorized!";
//    header('location:../../../index.php');
//}

?>