
<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="../Admin/dashboard.php"><img src="../../assets/images/logo_light.png" alt=""></a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../../assets/images/logo.jpg" alt="">
							<span>
<!--                            --><?php
//                               echo $_SESSION['user_info']['username'];
//                            ?>
							</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="" target="_blank"><i class="icon-user-plus"></i> My profile</a></li>
                    <li class="divider"></li>
                    <li><a href=""><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="../../assets/images/logo.jpg"
                                                                class="img-circle img-sm" alt=""></a>

                            <div class="media-body">
										<span class="media-heading text-semibold">
<!--                                            --><?php
//                                            echo $_SESSION['user_info']['username'];
//                                            ?>
										</span>

                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> &nbsp;Dhaka,Bangladesh
                                </div>
                            </div>

                            <div class="media-right media-middle"></div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->

<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
<!--            --><?php //if($_SESSION['user_info']['user_role']==1){ ?>
            <li class="active"><a href="../Admin/dashboard.php"><i class="icon-home4"></i>
                    <span>Dashboard</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-university"></i><span>Departments</span></a>
                <ul>
                    <li><a href="../Department/view.php">All Departments</a></li>
                    <li><a href="../Department/create.php">Add New Department</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-stack-star"></i> <span>Courses</span></a>
                <ul>
                    <li><a href="../Course/view.php">All Courses</a></li>
                    <li><a href="../Course/create.php">Add New Course</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-users"></i><span>Teachers</span></a>
                <ul>
                    <li><a href="../Teacher/view.php">All Teacher</a></li>
                    <li><a href="../Teacher/create.php">Add New Teacher</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-users4"></i><span>Register Students</span></a>
                <ul>
                    <li><a href="../Student/view.php">All Student</a></li>
                    <li><a href="../Student/create.php">Add New Student</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-book"></i><span>Course-Assign-To-Teacher</span></a>
                <ul>
                    <li><a href="../CourseAssignToTeacher/view.php">View Course Static</a></li>
                    <li><a href="../CourseAssignToTeacher/create.php">Add New</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-home7"></i><span>Class Rooms & Schedule</span></a>
                <ul>
                    <li><a href="../AllocateClassroom/view.php">View Class Schedule and Room Allocation</a></li>
                    <li><a href="../AllocateClassroom/create.php">Allocate Classrooms</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-user-plus"></i><span>Enroll In a Course</span></a>
                <ul>
                    <li><a href="../EnrollCourse/view.php">List Of Enroll Courses</a></li>
                    <li><a href="../EnrollCourse/create.php">Save Enroll Course</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-user-plus"></i><span>Save Student Result</span></a>
                <ul>
                    <li><a href="../SaveResult/view.php">View Result</a></li>
                    <li><a href="../SaveResult/create.php">Save Result</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-user-plus"></i><span>Student Result</span></a>
                <ul>
                    <li><a href="../ViewResult/view.php">View Result</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-user-plus"></i><span>Unassign All Courses</span></a>
                <ul>
                    <li><a href="../UnassignCourse/create.php">Unassign Courses</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-user-plus"></i><span>Unallocate All Classrooms</span></a>
                <ul>
                    <li><a href="../UnallocateRoom/create.php">Unallocate Rooms</a></li>
                </ul>
            </li>
        </ul>
    </div>
<?php //}else{?>

<!--            --><?php //}?>

</div>
</div>
</div>