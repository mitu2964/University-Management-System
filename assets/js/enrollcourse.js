$(document).ready(function () {
    $("#student_id").change(function () {
        var student_id = $(this).val();
        console.log(student_id);
        $.ajax({
            url: 'getStudent.php',
            type: 'post',
            data: {student_id: student_id},
            dataType: 'json',
            success: function (response) {
                console.log(response);
                console.log(response['student']);
                console.log(response['course']);

                var clen = response['course'].length;

                var title = response['student']['title'];
                $("#student_name").empty();

                var mail = response['student']['mail'];
                $("#student_mail").empty();

                var dept = response['student']['dept_title'];
                $("#student_dept").empty();

                $("#student_name").val(title);
                $("#student_mail").val(mail);
                $("#student_dept").val(dept);

                $("#course").empty();
                $("#course").append("<option value=''>" + 'Select Course Code' + "</option>");

                for (var i = 0; i < clen; i++) {
                    var id = response['course'][i]['course_id'];
                    var code = response['course'][i]['course_title'];
                    $("#course").append("<option value='" + id + "'>" + code + "</option>");
                }
            }
        });
    });
});